# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the flare package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: flare\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-20 16:48+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: data/resources/ui/about.ui:14
msgid "translator-credits"
msgstr ""

#: data/resources/ui/attachment.ui:26
msgid "Load image"
msgstr ""

#: data/resources/ui/attachment.ui:37
msgid "Load video"
msgstr ""

#: data/resources/ui/attachment.ui:48
msgid "Load file"
msgstr ""

#: data/resources/ui/attachment.ui:131
msgctxt "accessibility"
msgid "Open with…"
msgstr ""

#: data/resources/ui/attachment.ui:147
msgctxt "accessibility"
msgid "Download"
msgstr ""

#: data/resources/ui/channel_info_dialog.ui:7 data/resources/ui/window.ui:172
msgid "Channel Information"
msgstr ""

#: data/resources/ui/channel_info_dialog.ui:33
msgctxt "accessibility"
msgid "Reset Session"
msgstr ""

#: data/resources/ui/channel_info_dialog.ui:35
msgid "Reset Session"
msgstr ""

#: data/resources/ui/channel_info_dialog.ui:51
#: data/resources/ui/error_dialog.ui:14
msgid "Close"
msgstr ""

#: data/resources/ui/channel_list.ui:15
#: data/resources/ui/channel_messages.ui:17
msgid "Offline"
msgstr ""

#: data/resources/ui/channel_list.ui:31
msgid "Search"
msgstr ""

#: data/resources/ui/channel_list.ui:33 data/resources/ui/window.ui:58
msgctxt "accessibility"
msgid "Search"
msgstr ""

#: data/resources/ui/channel_messages.ui:31
msgid "No Channel Selected"
msgstr ""

#: data/resources/ui/channel_messages.ui:32
msgid "Select a channel"
msgstr ""

#: data/resources/ui/channel_messages.ui:139
msgctxt "accessibility"
msgid "Remove the reply"
msgstr ""

#: data/resources/ui/channel_messages.ui:141
msgctxt "tooltip"
msgid "Remove reply"
msgstr ""

#: data/resources/ui/channel_messages.ui:171
msgctxt "accessibility"
msgid "Remove an attachment"
msgstr ""

#: data/resources/ui/channel_messages.ui:173
msgctxt "tooltip"
msgid "Remove attachment"
msgstr ""

#: data/resources/ui/channel_messages.ui:207
msgctxt "accessibility"
msgid "Add an attachment"
msgstr ""

#: data/resources/ui/channel_messages.ui:209
msgctxt "tooltip"
msgid "Add attachment"
msgstr ""

#: data/resources/ui/channel_messages.ui:228
msgctxt "accessibility"
msgid "Insert an emoji"
msgstr ""

#: data/resources/ui/channel_messages.ui:230
msgctxt "tooltip"
msgid "Insert emoji"
msgstr ""

#: data/resources/ui/channel_messages.ui:238
msgctxt "accessibility"
msgid "Message input"
msgstr ""

#: data/resources/ui/channel_messages.ui:243
msgctxt "tooltip"
msgid "Message input"
msgstr ""

#: data/resources/ui/channel_messages.ui:249
msgctxt "accessibility"
msgid "Send"
msgstr ""

#: data/resources/ui/channel_messages.ui:251
msgctxt "tooltip"
msgid "Send"
msgstr ""

#: data/resources/ui/channel_messages.ui:252
msgid "Send"
msgstr ""

#: data/resources/ui/dialog_clear_messages.ui:4 data/resources/ui/window.ui:156
msgid "Clear messages"
msgstr ""

#: data/resources/ui/dialog_clear_messages.ui:5
msgid ""
"This will clear all locally stored messages. This will also close the "
"application."
msgstr ""

#: data/resources/ui/dialog_clear_messages.ui:9
#: data/resources/ui/dialog_unlink.ui:9 data/resources/ui/link_window.ui:16
msgid "Cancel"
msgstr ""

#: data/resources/ui/dialog_clear_messages.ui:10
msgid "Clear"
msgstr ""

#: data/resources/ui/dialog_unlink.ui:4 data/resources/ui/window.ui:152
msgid "Unlink"
msgstr ""

#: data/resources/ui/dialog_unlink.ui:5
msgid ""
"This will unlink this device and remove all locally saved data. This will "
"also close the application such that it can be relinked when opening it "
"again."
msgstr ""

#: data/resources/ui/dialog_unlink.ui:10
msgid "Unlink and delete all messages"
msgstr ""

#: data/resources/ui/dialog_unlink.ui:11
msgid "Unlink but keep messages"
msgstr ""

#: data/resources/ui/error_dialog.ui:8
msgid "Error"
msgstr ""

#: data/resources/ui/error_dialog.ui:59
msgid "Please consider reporting this error."
msgstr ""

#: data/resources/ui/link_window.ui:8
msgid "Link Device"
msgstr ""

#: data/resources/ui/link_window.ui:25
msgid "Please scan this QR code with your primary device."
msgstr ""

#: data/resources/ui/link_window.ui:35
msgctxt "accessibility"
msgid "Copy to clipboard"
msgstr ""

#: data/resources/ui/link_window.ui:47
msgid "Please wait until the contacts are displayed after linking."
msgstr ""

#: data/resources/ui/message_item.ui:248
msgid "Reply"
msgstr ""

#: data/resources/ui/message_item.ui:254
msgid "React"
msgstr ""

#: data/resources/ui/message_item.ui:260
msgid "Delete"
msgstr ""

#: data/resources/ui/message_item.ui:267
msgid "Copy"
msgstr ""

#: data/resources/ui/preferences_window.ui:9
msgid "General"
msgstr ""

#: data/resources/ui/preferences_window.ui:13
msgid "Linking"
msgstr ""

#: data/resources/ui/preferences_window.ui:14
msgid "The device will need to be relinked to take effect."
msgstr ""

#: data/resources/ui/preferences_window.ui:17
msgid "Device Name"
msgstr ""

#: data/resources/ui/preferences_window.ui:25
msgid "Attachments"
msgstr ""

#: data/resources/ui/preferences_window.ui:28
msgid "Dowload images automatically"
msgstr ""

#: data/resources/ui/preferences_window.ui:42
msgid "Dowload videos automatically"
msgstr ""

#: data/resources/ui/preferences_window.ui:56
msgid "Dowload files automatically"
msgstr ""

#: data/resources/ui/preferences_window.ui:73
msgid "Notifications"
msgstr ""

#: data/resources/ui/preferences_window.ui:74
msgid "Notifications for new messages."
msgstr ""

#: data/resources/ui/preferences_window.ui:77
#: data/de.schmidhuberj.Flare.gschema.xml:37
msgid "Send notifications"
msgstr ""

#: data/resources/ui/preferences_window.ui:91 src/gui/preferences_window.rs:29
msgid "Watch for new messages while closed"
msgstr ""

#: data/resources/ui/preferences_window.ui:109
msgid "Other"
msgstr ""

#: data/resources/ui/preferences_window.ui:112
msgid "Selectable Message Text"
msgstr ""

#: data/resources/ui/preferences_window.ui:113
msgid "Selectable text can interfere with swipe-gestures on touch-screens."
msgstr ""

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr ""

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Show shortcuts"
msgstr ""

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Go to settings"
msgstr ""

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Go to about page"
msgstr ""

#: data/resources/ui/shortcuts.ui:32
msgctxt "shortcut window"
msgid "Open menu"
msgstr ""

#: data/resources/ui/shortcuts.ui:38
msgctxt "shortcut window"
msgid "Close window"
msgstr ""

#: data/resources/ui/shortcuts.ui:46
msgctxt "shortcut window"
msgid "Channels"
msgstr ""

#: data/resources/ui/shortcuts.ui:49
msgctxt "shortcut window"
msgid "Go to channel 1…9"
msgstr ""

#: data/resources/ui/shortcuts.ui:55
msgctxt "shortcut window"
msgid "Search in channels"
msgstr ""

#: data/resources/ui/shortcuts.ui:61
msgctxt "shortcut window"
msgid "Upload attachment"
msgstr ""

#: data/resources/ui/shortcuts.ui:67
msgctxt "shortcut window"
msgid "Focus the text input box"
msgstr ""

#: data/resources/ui/shortcuts.ui:73
msgctxt "shortcut window"
msgid "Load more messages"
msgstr ""

#: data/resources/ui/window.ui:36
msgctxt "accessibility"
msgid "Add Conversation"
msgstr ""

#: data/resources/ui/window.ui:46 data/resources/ui/window.ui:117
msgctxt "accessibility"
msgid "Menu"
msgstr ""

#: data/resources/ui/window.ui:148
msgid "Settings"
msgstr ""

#: data/resources/ui/window.ui:160
msgid "Keybindings"
msgstr ""

#: data/resources/ui/window.ui:164
msgid "About"
msgstr ""

#: src/backend/channel.rs:505
msgid "Note to self"
msgstr ""

#: src/backend/manager.rs:378
msgid "You"
msgstr ""

#: src/backend/message/call_message.rs:115
msgid "Started calling."
msgstr ""

#: src/backend/message/call_message.rs:116
msgid "Answered a call."
msgstr ""

#: src/backend/message/call_message.rs:117
msgid "Hung up."
msgstr ""

#: src/backend/message/call_message.rs:118
msgid "Is busy."
msgstr ""

#: src/backend/message/text_message.rs:294
msgid "Sent an image"
msgid_plural "Sent {} images"
msgstr[0] ""
msgstr[1] ""

#: src/backend/message/text_message.rs:297
msgid "Sent an video"
msgid_plural "Sent {} videos"
msgstr[0] ""
msgstr[1] ""

#: src/backend/message/text_message.rs:300
msgid "Sent a file"
msgid_plural "Sent {} files"
msgstr[0] ""
msgstr[1] ""

#: src/error.rs:87
msgid "I/O Error."
msgstr ""

#: src/error.rs:92
msgid "There does not seem to be a connection to the internet available."
msgstr ""

#: src/error.rs:97
msgid "Something glib-related failed."
msgstr ""

#: src/error.rs:102
msgid "The communication with Libsecret failed."
msgstr ""

#: src/error.rs:107
msgid ""
"The backend database failed. Please restart the application or delete the "
"database and relink the application."
msgstr ""

#: src/error.rs:112
msgid ""
"You do not seem to be authorized with Signal. Please delete the database and "
"relink the application."
msgstr ""

#: src/error.rs:117
msgid "Sending a message failed."
msgstr ""

#: src/error.rs:122
msgid "Receiving a message failed."
msgstr ""

#: src/error.rs:127
msgid ""
"Something unexpected happened with the signal backend. Please retry later."
msgstr ""

#: src/error.rs:132
msgid "The application seems to be misconfigured."
msgstr ""

#: src/error.rs:137
msgid "A part of the application crashed."
msgstr ""

#: src/error.rs:147
msgid "Please check your internet connection."
msgstr ""

#: src/error.rs:152
msgid "Please delete the database and relink the device."
msgstr ""

#: src/error.rs:159
msgid "The database path at {} is no folder."
msgstr ""

#: src/error.rs:164
msgid "Please restart the application with logging and report this issue."
msgstr ""

#: src/gui/preferences_window.rs:51
msgid "Background permission"
msgstr ""

#: src/gui/preferences_window.rs:52
msgid "Use settings to remove permissions"
msgstr ""

#. How to format time. Should probably be %H:%M (meaning print hours from 00-23, then a :,
#. then minutes from 00-59). For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:57
msgid "%H:%M"
msgstr ""

#. How to format a date with time. Should probably be similar to %Y-%m-%d %H:%M (meaning print year, month from 01-12, day from 01-31 (each separated by -), hours from 00-23, then a :,
#. then minutes from 00-59). For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:60
msgid "%Y-%m-%d %H:%M"
msgstr ""

#. Translators: Flare name in metainfo.
#: data/de.schmidhuberj.Flare.desktop.in.in:5
#: data/de.schmidhuberj.Flare.metainfo.xml:8
msgid "Flare"
msgstr ""

#. Translators: Flare summary in metainfo.
#: data/de.schmidhuberj.Flare.metainfo.xml:10
msgid "An unofficial Signal GTK client"
msgstr ""

#. Translators: Description of Flare in metainfo.
#: data/de.schmidhuberj.Flare.metainfo.xml:26
msgid ""
"An unofficial GTK client for the messaging application Signal. This is a "
"very simple application with many features missing compared to the official "
"applications."
msgstr ""

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:32
msgid "Link device"
msgstr ""

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:36
msgid "Send messages"
msgstr ""

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:40
msgid "Receive messages"
msgstr ""

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:44
msgid "Reply to a message"
msgstr ""

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:48
msgid "React to a message"
msgstr ""

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:52
msgid "Sending and receiving attachments"
msgstr ""

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:56
msgid "Encrypted storage"
msgstr ""

#. Translators: Description of Flare in metainfo: Features
#: data/de.schmidhuberj.Flare.metainfo.xml:60
msgid "Notifications, optionally in the background"
msgstr ""

#. Translators: Description of Flare in metainfo: Security note
#: data/de.schmidhuberj.Flare.metainfo.xml:65
msgid ""
"More features are planned, blocked or not-planned. Consult the README for "
"more information about them."
msgstr ""

#. Translators: Description of Flare in metainfo: Security note
#: data/de.schmidhuberj.Flare.metainfo.xml:69
msgid ""
"Please note that using this application will probably worsen your security "
"compared to using official Signal applications. Use with care when handling "
"sensitive data. Look at the projects README for more information about "
"security."
msgstr ""

#: data/de.schmidhuberj.Flare.metainfo.xml:397
msgid "Overview of the application"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:6
msgid "Window width"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:10
msgid "Window height"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:14
msgid "Window maximized state"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:19
msgid ""
"The device name when linking Flare. This requires the application to be re-"
"linked."
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:24
msgid "Automatically download images"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:28
msgid "Automatically download videos"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:32
msgid "Automatically download files"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:41
msgid "Run in background when closed"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:46
msgid "Can messages be selected"
msgstr ""
